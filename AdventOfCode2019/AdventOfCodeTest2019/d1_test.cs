using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdventOfCode2019.days;
using System;

namespace AdventOfCodeTest2019
{
    [TestClass]
    public class D1_test
    {
        [TestMethod]
        public void TestCalc1()
        {
            Assert.AreEqual(4, D1.Calc1(20));
            Assert.AreEqual(331, D1.Calc1(999));
        }

        [TestMethod]
        public void TestCalc2()
        {
            Assert.AreEqual(2, D1.Calc2(14));
            Assert.AreEqual(966, D1.Calc2(1969));
            Assert.AreEqual(50346, D1.Calc2(100756));
        }
    }
}
