﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdventOfCode2019.days;
using System;
using System.IO;
using System.Linq;

namespace AdventOfCodeTest2019
{
    [TestClass]
    public class D2_test
    {
        [TestMethod]
        public void TestPart1()
        {
            StreamReader sr = new StreamReader("input/02.txt");
            int[] data = sr.ReadLine().Split(',').Select(x => Int32.Parse(x)).ToArray();

            Assert.AreEqual(5110675, D2.Calc(data, 12, 2));
        }

        [TestMethod]
        public void TestPart2()
        {
            StreamReader sr = new StreamReader("input/02.txt");
            int[] data = sr.ReadLine().Split(',').Select(x => Int32.Parse(x)).ToArray();

            Assert.AreEqual(19690720, D2.Calc(data, 48, 47));
        }
    }
}
