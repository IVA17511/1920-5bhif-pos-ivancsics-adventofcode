﻿using AdventOfCode2019.days;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCodeTest2019
{
    [TestClass]
    class D4_test
    {
        [TestMethod]
        public void TestPart1()
        {
            Assert.AreEqual(true, D4.PossiblePassword(111111, false));
            Assert.AreEqual(false, D4.PossiblePassword(223450, false));
            Assert.AreEqual(false, D4.PossiblePassword(123789, false));
        }

        [TestMethod]
        public void TestPart2()
        {
            Assert.AreEqual(true, D4.PossiblePassword(112233, true));
            Assert.AreEqual(false, D4.PossiblePassword(123444, true));
            Assert.AreEqual(true, D4.PossiblePassword(111122, true));
        }
    }
}
