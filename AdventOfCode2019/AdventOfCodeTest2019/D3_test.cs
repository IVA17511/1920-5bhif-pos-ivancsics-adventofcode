﻿using AdventOfCode2019.days;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCodeTest2019
{
    [TestClass]
    public class D3_test
    {
        [TestMethod]
        public void TestPart1()
        {
            Assert.AreEqual(159, D3.Calc("input/03_a.txt", 1));
            Assert.AreEqual(135, D3.Calc("input/03_b.txt", 1));
        }

        [TestMethod]
        public void TestPart2()
        {
            Assert.AreEqual(610, D3.Calc("input/03_a.txt", 2));
            Assert.AreEqual(410, D3.Calc("input/03_b.txt", 2));
        }
    }
}
