﻿using AdventOfCode2019.days;
using System;

namespace AdventOfCode2019
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Day 1:");
            new D1().Run();

            Console.WriteLine();
            Console.WriteLine("Day 2:");
            new D2().Run();

            Console.WriteLine();
            Console.WriteLine("Day 3:");
            new D3().Run();

            Console.WriteLine();
            Console.WriteLine("Day 4:");
            new D4().Run();

            Console.WriteLine();
            Console.WriteLine("Day 8:");
            new D8().Run();

            Console.ReadLine();
        }
    }
}
