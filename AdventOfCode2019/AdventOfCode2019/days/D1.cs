﻿using System;
using System.IO;
using System.Linq;

namespace AdventOfCode2019.days
{
    public class D1
    {
        public void Run()
        {
            var input = File.ReadAllLines("input/01.txt").Select(int.Parse).ToArray();

            Console.WriteLine("Part 1: " + input.Sum(Calc1));
            Console.WriteLine("Part 2: " + input.Sum(Calc2));
        }

        public static int Calc1(int x)
        {
            return x / 3 - 2;
        }
        
        public static int Calc2(int x)
        {
            int total = 0;
            int fuel = x;
            while (fuel / 3 - 2 > 0)
            {
                fuel = (fuel / 3 - 2);
                total += fuel;
            }
            return total;
        }
    }
}
