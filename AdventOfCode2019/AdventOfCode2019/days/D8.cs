﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2019.days
{
    class D8
    {
        const int length = 25;
        const int height = 6;

        public void Run()
        {
            Console.WriteLine("Part 1: " + Calc("input/08.txt", false));
            Console.WriteLine("Part 2: ");
            Calc("input/08.txt", true);
        }

        public static int Calc(String input, bool part2)
        {
            StreamReader sr = new StreamReader(input);
            String image = sr.ReadLine();

            var layers = CreateLayers(image);

            int fewestZeroes = int.MaxValue;
            int[,] l = null;
            foreach (var layer in layers)
            {
                int currZeroes = CountPixel(layer, 0);
                if (currZeroes < fewestZeroes)
                {
                    fewestZeroes = currZeroes;
                    l = layer;
                }
            }

            if(!part2)
                return (CountPixel(l, 1) * CountPixel(l, 2));
            else
                Print3DLayers(layers);
            return 0;
        }

        private static List<int[,]> CreateLayers(String input)
        {
            List<int[,]> layers = new List<int[,]>();

            int[,] currentLayer = new int[length, height];
            int currLength = 0;
            int currHeight = 0;

            foreach (char c in input)
            {
                currentLayer[currLength, currHeight] = int.Parse(c.ToString());
                currLength++;

                if (currLength == length)
                {
                    currLength = 0;
                    currHeight++;
                    if (currHeight == height)
                    {
                        currHeight = 0;
                        layers.Add(currentLayer);
                        currentLayer = new int[length, height];
                    }
                }
            }
            layers.Add(currentLayer);
            return layers;
        }

        private static int CountPixel(int[,] layer, int p)
        {
            int count = 0;

            for (int i = 0; i < layer.GetLength(1); i++)
                for (int j = 0; j < layer.GetLength(0); j++)
                    if (layer[j, i] == p)
                        count++;

            return count;
        }

        private static void Print3DLayers(List<int[,]> layers)
        {
            for (int i = 0; i < layers[0].GetLength(1); i++)
            {
                Console.WriteLine();

                for (int j = 0; j < layers[0].GetLength(0); j++) // go through each pixel and paint it directly
                {
                    int layer = 0;
                    while (layers[layer][j, i] == 2) // skip transparents
                        layer++;

                    if (layers[layer][j, i] == 1)
                        Console.ForegroundColor = ConsoleColor.White;
                    else
                        Console.ForegroundColor = ConsoleColor.Black;

                    Console.Write("■");
                }
            }
        }
    }
}
